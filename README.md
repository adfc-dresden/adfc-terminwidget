# adfc-terminwidget

Terminkalender-IFRAME für ADFC Radtouren- und Terminportal.

## Wie sieht es aus?

Wird z.B. bei https://adfc-dresden.de/index.php/termine verwendet. Der reine
Inhalt des IFRAMEs ist findet sich unter
https://adfc-dresden.de/adfc-termine.html.

## Lizenz

Das Widget selbst kann steht unter der [Creative Commons
Zero-Lizenz](https://creativecommons.org/publicdomain/zero/1.0/) und kann
dadurch frei verwedet werden. Es verwendet die
[jQuery-Bibliothek](https://jquery.com/), die unter der MIT-Lizenz frei
verwendet werden kann und zur Bequemlichkeit eine Kopie mitgeliefert wird.
